/**************************************************************************************
 * This file is written by Mads Bornebusch
 * It is based on ViewPagerAdapter.java by Kristian Lauszus:
 * https://github.com/Lauszus/BalancingRobotFullSizeAndroid
 *
 * This software may be distributed and modified under the terms of the GNU
 * General Public License version 2 (GPL2) as published by the Free Software
 * Foundation and appearing in the file GPL2.TXT included in the packaging of
 * this file. Please note that GPL2 Section 2[b] requires that all works based
 * on this software must also be made publicly available under the terms of
 * the GPL2 ("Copyleft").
 **************************************************************************************/
package madsbornebusch.imupositiontrackingapp;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    public static final int INFO_FRAGMENT = 0;
    public static final int GRAPH_FRAGMENT = 1;
    public static final int MAPS_FRAGMENT = 2;


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case INFO_FRAGMENT:
                return new InfoFragment();
            case GRAPH_FRAGMENT:
                return new GraphFragment();
            case MAPS_FRAGMENT:
                return new MapsFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        return 3; // Return number of tabs
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case INFO_FRAGMENT:
                return "Info";
            case GRAPH_FRAGMENT:
                return "Graph";
            case MAPS_FRAGMENT:
                return "Map";

        }
        return null;
    }

}
