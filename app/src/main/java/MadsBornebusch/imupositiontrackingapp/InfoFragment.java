/**************************************************************************************
 * This file is written by Mads Bornebusch, 2015.
 *
 * It is inspired by InfoFragment.java by Kristian Lauszus:
 * https://github.com/Lauszus/BalancingRobotFullSizeAndroid
 *
 * This software may be distributed and modified under the terms of the GNU
 * General Public License version 2 (GPL2) as published by the Free Software
 * Foundation and appearing in the file GPL2.TXT included in the packaging of
 * this file. Please note that GPL2 Section 2[b] requires that all works based
 * on this software must also be made publicly available under the terms of
 * the GPL2 ("Copyleft").
 **************************************************************************************/

package madsbornebusch.imupositiontrackingapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.Arrays;

import madsbornebusch.imupositiontrackingapp.R;


public class InfoFragment extends android.support.v4.app.Fragment {

     TextView  mAngle, mAcceleration, mVelocity, mPosition;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_info, container, false);

        if (v == null) {
            throw new RuntimeException();
        }

        // Customize View
        mAngle = (TextView) v.findViewById(R.id.angle);
        mAcceleration = (TextView) v.findViewById(R.id.acceleration);
        mVelocity = (TextView) v.findViewById(R.id.velocity);
        mPosition = (TextView) v.findViewById(R.id.position);

        return v;
    }


    public void updateView(float[] quatAngle, float[] acceleration, float[] velocity, float[] position ) {
        double[] angle = new double[3];
        // Source for the calculation: http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        angle[0] = Math.toDegrees(Math.atan2(2.0 * (quatAngle[0] * quatAngle[1] + quatAngle[2] * quatAngle[3]), 1.0 - 2.0 * (quatAngle[1] * quatAngle[1] + quatAngle[2] * quatAngle[2])));
        angle[1] = Math.toDegrees(Math.asin(2.0 * (quatAngle[0] * quatAngle[2] - quatAngle[3] * quatAngle[1])));
        angle[2] = Math.toDegrees(Math.atan2(2.0 * (quatAngle[0] * quatAngle[3] + quatAngle[1] * quatAngle[2]), 1.0 - 2.0 * (quatAngle[2] * quatAngle[2] + quatAngle[3] * quatAngle[3])));
        // Rounding numbers
        angle[0] = Math.round(angle[0] * 100.0) / 100.0;
        angle[1] = Math.round(angle[1] * 100.0) / 100.0;
        angle[2] = Math.round(angle[2] * 100.0) / 100.0;

        if (mAngle != null){
            mAngle.setText(Arrays.toString(angle));
        }

        if (mAcceleration != null){
            mAcceleration.setText(Arrays.toString(acceleration));
        }

        if (mVelocity != null){
            mVelocity.setText(Arrays.toString(velocity));
        }

        if (mPosition != null){
            mPosition.setText(Arrays.toString(position));
        }
    }
}
