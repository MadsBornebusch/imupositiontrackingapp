/**************************************************************************************
 * This file is written by Mads Bornebusch, 2015.
 *
 * It is inspired by MapFragment.java by Kristian Lauszus:
 * https://github.com/Lauszus/BalancingRobotFullSizeAndroid
 *
 * This software may be distributed and modified under the terms of the GNU
 * General Public License version 2 (GPL2) as published by the Free Software
 * Foundation and appearing in the file GPL2.TXT included in the packaging of
 * this file. Please note that GPL2 Section 2[b] requires that all works based
 * on this software must also be made publicly available under the terms of
 * the GPL2 ("Copyleft").
 **************************************************************************************/
package madsbornebusch.imupositiontrackingapp;

import android.graphics.Color;
import android.util.Log;
import madsbornebusch.imupositiontrackingapp.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapsFragment extends SupportMapFragment implements OnMapLongClickListener {
    private static final String TAG = "MapsFragment";
    private static final boolean D = IMUPositionTrackingHome.D;
    private static double radiusEarth = 6371009.0;

    private GoogleMap mMap;
    private Polyline line;
    private PolylineOptions lineOptions;
    private Polyline lineGPS;
    private PolylineOptions lineOptionsGPS;

    private boolean initClick = true;
    private boolean initAngle = true;
    private double[] startAngle;

    private float[] lastPos = {0,0,0};
    private float[] deltaPos = {0,0,0};

    private double pointLat;
    private double pointLong;


    @Override
    public void onResume() {
        super.onResume();
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            new AlertDialog.Builder(getActivity())
                    .setMessage("Your GPS seems to be disabled, do you want to enable it?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, final int id) {
                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                    .setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, final int id) {
                                    dialog.cancel();
                                    Toast.makeText(getActivity(), "GPS must be on in order to use this application!", Toast.LENGTH_LONG).show();
                                    getActivity().finish();
                                }
                            })
                    .create().show();
        } else {
            setUpMapIfNeeded();

            // Get location
            Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));
            if (location != null) {
                LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 19.0f));
            } else {
                Toast.makeText(getActivity(), "GPS location not available!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            mMap = getMap(); // Obtain the map
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setAllGesturesEnabled(true);
                mMap.setOnMapLongClickListener(this);
            } else {
                Toast.makeText(getActivity(), "Google Maps is not available!", Toast.LENGTH_LONG).show();
                getActivity().finish();
            }
        }
    }

    @Override
    public void onMapLongClick(LatLng point) {

        if(initClick){
            initClick = false;

            // Add marker where the user clicked
            mMap.addMarker(new MarkerOptions().position(point).title(point.toString())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            // Create a line on the map to show positions from Bluetooth
            lineOptions = new PolylineOptions()
                    .add(point)
                    .width(3)
                    .color(Color.GREEN);
            // Store the coordinates of the selected starting point
            pointLat = point.latitude;
            pointLong = point.longitude;

            // Getting the GPS location
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));
            if (location != null) {
                // Add marker at the GPS position
                LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.addMarker(new MarkerOptions().position(myLocation).title("GPS initial position")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                // Create a line to show GPS positions
                lineOptionsGPS = new PolylineOptions()
                        .add(myLocation)
                        .width(3)
                        .color(Color.BLUE);
            }
        }
    }


    public void updateView(float[] quatAngle, float[] acceleration, float[] velocity, float[] position ) {

        // Saving the first angle
        if(!initClick && initAngle){
            startAngle = float2doubleArray(quatAngle);
            lastPos[0] = position[0];
            lastPos[1] = position[1];
            lastPos[2] = position[2];

            initAngle = false;

        }

        // Subtracting the last position from the current position
        deltaPos[0] = position[0] - lastPos[0];
        deltaPos[1] = position[1]- lastPos[1];
        deltaPos[2] = position[2] - lastPos[2];

        // Updating the last position
        lastPos[0] = position[0];
        lastPos[1] = position[1];
        lastPos[2] = position[2];

        if (D) {
            Log.d(TAG, "delta x " + deltaPos[0]);
            Log.d(TAG, "delta y " + deltaPos[1]);
            Log.d(TAG, "delta z " + deltaPos[2]);

        }
        // Check if the startAngle has been set
        if(startAngle!= null){
            // Move the position into an array of size 4
            double[] sensorPos ={0,0,0,0};
            System.arraycopy(float2doubleArray(deltaPos),0,sensorPos,1,3);

            // Using the sensor position directly. This means the sensor needs to point north when the initial position is set by long clicking the map
           // double[] earthPos = sensorPos;
            // Not rotating the position vector due to the low resolution in the rotation.
            // To rotate the position vector with the startAngle use this line instead:
            double[] earthPos = quatProd(quatProd(startAngle,sensorPos),quatConj(startAngle));

            /** NOTE: Maybe the old position in sensor frame can be subtracted from the new
             *  position and only the difference rotated?
             *  It seemed that the error was proportional to the length of the rotated vector.
             *  Or the initial heading can be set manually in the app?
             */

            // Calculate the latitude and longitude from the position data.
            pointLat += Math.toDegrees(Math.atan((earthPos[1])/radiusEarth));
            pointLong -= Math.toDegrees(Math.atan((earthPos[2])/radiusEarth));


            // Add the new position to line with positions from Bluetooth
            lineOptions.add(new LatLng(pointLat,pointLong));
            line = mMap.addPolyline(lineOptions);

            // Add a new location to the line with GPS coordinates
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));
            if (location != null) {
                LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                lineOptionsGPS.add(myLocation);
                lineGPS = mMap.addPolyline(lineOptionsGPS);
            }else{
                Toast.makeText(getActivity(), "GPS location not available!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private double[] quatProd(double[] a, double[] b){
        // Source: http://www.x-io.co.uk/res/doc/quaternions.pdf
        // Common sense and rules for ijk-multiplication from http://www.emis.de/proceedings/Varna/vol1/GEOM09.pdf
        // This is a x b. This product is NOT commutative.
        // Multiplication of quaternions is associative: http://en.wikipedia.org/wiki/Associative_property
        double[] q = new double[4];
        q[0] = a[0]*b[0] - a[1]*b[1] - a[2]*b[2] - a[3]*b[3];
        q[1] = a[0]*b[1] + a[1]*b[0] + a[2]*b[3] - a[3]*b[2];
        q[2] = a[0]*b[2] - a[1]*b[3] + a[2]*b[0] + a[3]*b[1];
        q[3] = a[0]*b[3] + a[1]*b[2] - a[2]*b[1] + a[3]*b[0];
        return q;
    }

    private double[] quatConj(double[] q){
        //Source: Page 3 of http://www.emis.de/proceedings/Varna/vol1/GEOM09.pdf
        q[1] = -q[1];
        q[2] = -q[2];
        q[3] = -q[3];
        return q;
    }

    private double[] float2doubleArray(float[] input){
        if (input == null){
            return null;
        }
        double[] output = new double[input.length];
        for (int i = 0; i < input.length; i++){
            output[i] = input[i];
        }
        return output;
    }

}



