/**************************************************************************************
 * This file is written by Mads Bornebusch, 2015.
 *
 * It is inspired by GraphFragment.java by Kristian Lauszus:
 * https://github.com/Lauszus/BalancingRobotFullSizeAndroid
 *
 * This software may be distributed and modified under the terms of the GNU
 * General Public License version 2 (GPL2) as published by the Free Software
 * Foundation and appearing in the file GPL2.TXT included in the packaging of
 * this file. Please note that GPL2 Section 2[b] requires that all works based
 * on this software must also be made publicly available under the terms of
 * the GPL2 ("Copyleft").
 **************************************************************************************/

package madsbornebusch.imupositiontrackingapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.graphics.Color;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphView.LegendAlign;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;
import com.jjoe64.graphview.GraphViewStyle;
import com.jjoe64.graphview.LineGraphView;

import madsbornebusch.imupositiontrackingapp.R;

public class GraphFragment extends android.support.v4.app.Fragment {
    private static final String TAG = "GraphFragment";
    private static final boolean D = IMUPositionTrackingHome.D;

    private static final int NUM_POINTS = 100;

    private static LineGraphView graphView;

    private static GraphViewSeries angleXseries, angleYseries, angleZseries;
    private static GraphViewSeries accXseries, accYseries, accZseries;
    private static GraphViewSeries velXseries, velYseries , velZseries;
    private static GraphViewSeries posXseries, posYseries, posZseries;

    private static double counter = 0.0;

    private static CheckBox mCheckBoxAngleAll, mCheckBoxAngleX, mCheckBoxAngleY, mCheckBoxAngleZ;
    private static CheckBox mCheckBoxAccAll, mCheckBoxAccX, mCheckBoxAccY, mCheckBoxAccZ;
    private static CheckBox mCheckBoxVelAll, mCheckBoxVelX, mCheckBoxVelY, mCheckBoxVelZ;
    private static CheckBox mCheckBoxPosAll, mCheckBoxPosX, mCheckBoxPosY, mCheckBoxPosZ;

    public GraphFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_graph, container, false);

        if (v == null){
            throw new RuntimeException();
        }

        GraphViewData[] data0 = new GraphViewData[1];

        data0[0] = new GraphViewData(0,0);


        angleXseries = new GraphViewSeries("Angle X", new GraphViewSeriesStyle(Color.RED, 2), data0);
        angleYseries = new GraphViewSeries("Angle Y", new GraphViewSeriesStyle(Color.GREEN, 2), data0);
        angleZseries = new GraphViewSeries("Angle Z", new GraphViewSeriesStyle(Color.BLUE, 2), data0);

        accXseries = new GraphViewSeries("Acc. X", new GraphViewSeriesStyle(Color.rgb(0,250,250), 2), data0);
        accYseries = new GraphViewSeries("Acc. Y", new GraphViewSeriesStyle(Color.rgb(250,250,0), 2), data0);
        accZseries = new GraphViewSeries("Acc. Z", new GraphViewSeriesStyle(Color.rgb(250,0,250), 2), data0);

        velXseries = new GraphViewSeries("Vel. X", new GraphViewSeriesStyle(Color.rgb(0,175,175), 2), data0);
        velYseries = new GraphViewSeries("Vel. Y", new GraphViewSeriesStyle(Color.rgb(175,175,0), 2), data0);
        velZseries = new GraphViewSeries("Vel. Z", new GraphViewSeriesStyle(Color.rgb(175,0,175), 2), data0);

        posXseries = new GraphViewSeries("Pos. X", new GraphViewSeriesStyle(Color.rgb(0,100,100), 2), data0);
        posYseries = new GraphViewSeries("Pos. Y", new GraphViewSeriesStyle(Color.rgb(100,100,0), 2), data0);
        posZseries = new GraphViewSeries("Pos. Z", new GraphViewSeriesStyle(Color.rgb(100,0,100), 2), data0);


        graphView = new LineGraphView(getActivity(), "");
        graphView.addSeries(angleXseries);
        graphView.addSeries(angleYseries);
        graphView.addSeries(angleZseries);

        //graphView.setManualYAxisBounds(180, -180);
        graphView.setViewPort(0, NUM_POINTS);
        graphView.setScrollable(true);
        graphView.setDisableTouch(true);

        graphView.setShowLegend(true);
        graphView.setLegendAlign(LegendAlign.BOTTOM);
        graphView.scrollToEnd();

        LinearLayout layout = (LinearLayout) v.findViewById(R.id.linegraph);

        GraphViewStyle mGraphViewStyle = new GraphViewStyle();
        //mGraphViewStyle.setNumHorizontalLabels(11);
        //mGraphViewStyle.setNumVerticalLabels(11);
        mGraphViewStyle.setTextSize(15);
        mGraphViewStyle.setLegendWidth(140);
        mGraphViewStyle.setLegendMarginBottom(30);

        graphView.setGraphViewStyle(mGraphViewStyle);

        layout.addView(graphView);

        // Checkbox for ANGLE all
        mCheckBoxAngleAll = (CheckBox) v.findViewById(R.id.checkBoxAngleAll);
        mCheckBoxAngleAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    mCheckBoxAngleX.setEnabled(true);
                    mCheckBoxAngleY.setEnabled(true);
                    mCheckBoxAngleZ.setEnabled(true);

                    if (mCheckBoxAngleX.isChecked()) {
                        graphView.addSeries(angleXseries);
                    }else {
                        graphView.removeSeries(angleXseries);
                    }
                    if (mCheckBoxAngleY.isChecked()) {
                        graphView.addSeries(angleYseries);
                    }else {
                        graphView.removeSeries(angleYseries);
                    }
                    if (mCheckBoxAngleZ.isChecked()) {
                        graphView.addSeries(angleZseries);
                    }else{
                        graphView.removeSeries(angleZseries);
                    }
                }else{
                    mCheckBoxAngleX.setEnabled(false);
                    mCheckBoxAngleY.setEnabled(false);
                    mCheckBoxAngleZ.setEnabled(false);
                    graphView.removeSeries(angleXseries);
                    graphView.removeSeries(angleYseries);
                    graphView.removeSeries(angleZseries);

                }
            }
        });

        // Checkbox for ANGLE X
        mCheckBoxAngleX = (CheckBox) v.findViewById(R.id.checkBoxAngleX);
        mCheckBoxAngleX.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(angleXseries);
                    }else {
                        graphView.removeSeries(angleXseries);
                    }
                }
            }
        });

        // Checkbox for ANGLE Y
        mCheckBoxAngleY = (CheckBox) v.findViewById(R.id.checkBoxAngleY);
        mCheckBoxAngleY.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(angleYseries);
                    }else {
                        graphView.removeSeries(angleYseries);
                    }
                }
            }
        });

        // Checkbox for ANGLE Z
        mCheckBoxAngleZ = (CheckBox) v.findViewById(R.id.checkBoxAngleZ);
        mCheckBoxAngleZ.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(angleZseries);
                    }else {
                        graphView.removeSeries(angleZseries);
                    }
                }
            }
        });

        // Checkbox for ACCELERATION all
        mCheckBoxAccAll = (CheckBox) v.findViewById(R.id.checkBoxAccAll);
        mCheckBoxAccAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    mCheckBoxAccX.setEnabled(true);
                    mCheckBoxAccY.setEnabled(true);
                    mCheckBoxAccZ.setEnabled(true);

                    if (mCheckBoxAccX.isChecked()) {
                        graphView.addSeries(accXseries);
                    }else {
                        graphView.removeSeries(accXseries);
                    }
                    if (mCheckBoxAccY.isChecked()) {
                        graphView.addSeries(accYseries);
                    }else {
                        graphView.removeSeries(accYseries);
                    }
                    if (mCheckBoxAccZ.isChecked()) {
                        graphView.addSeries(accZseries);
                    }else{
                        graphView.removeSeries(accZseries);
                    }
                }else{
                    mCheckBoxAccX.setEnabled(false);
                    mCheckBoxAccY.setEnabled(false);
                    mCheckBoxAccZ.setEnabled(false);
                    graphView.removeSeries(accXseries);
                    graphView.removeSeries(accYseries);
                    graphView.removeSeries(accZseries);
                }
            }
        });

        // Checkbox for ACCELERATION X
        mCheckBoxAccX = (CheckBox) v.findViewById(R.id.checkBoxAccX);
        mCheckBoxAccX.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(accXseries);
                    }else {
                        graphView.removeSeries(accXseries);
                    }
                }
            }
        });

        // Checkbox for ACCELERATION Y
        mCheckBoxAccY = (CheckBox) v.findViewById(R.id.checkBoxAccY);
        mCheckBoxAccY.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(accYseries);
                    }else {
                        graphView.removeSeries(accYseries);
                    }
                }
            }
        });

        // Checkbox for ACCELERATION Z
        mCheckBoxAccZ = (CheckBox) v.findViewById(R.id.checkBoxAccZ);
        mCheckBoxAccZ.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(accZseries);
                    }else {
                        graphView.removeSeries(accZseries);
                    }
                }
            }
        });


        // Checkbox for VELOCITY all
        mCheckBoxVelAll = (CheckBox) v.findViewById(R.id.checkBoxVelAll);
        mCheckBoxVelAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    mCheckBoxVelX.setEnabled(true);
                    mCheckBoxVelY.setEnabled(true);
                    mCheckBoxVelZ.setEnabled(true);

                    if (mCheckBoxVelX.isChecked()) {
                        graphView.addSeries(velXseries);
                    }else {
                        graphView.removeSeries(velXseries);
                    }
                    if (mCheckBoxVelY.isChecked()) {
                        graphView.addSeries(velYseries);
                    }else {
                        graphView.removeSeries(velYseries);
                    }
                    if (mCheckBoxVelZ.isChecked()) {
                        graphView.addSeries(velZseries);
                    }else{
                        graphView.removeSeries(velZseries);
                    }
                }else{
                    mCheckBoxVelX.setEnabled(false);
                    mCheckBoxVelY.setEnabled(false);
                    mCheckBoxVelZ.setEnabled(false);
                    graphView.removeSeries(velXseries);
                    graphView.removeSeries(velYseries);
                    graphView.removeSeries(velZseries);
                }
            }
        });

        // Checkbox for VELOCITY X
        mCheckBoxVelX = (CheckBox) v.findViewById(R.id.checkBoxVelX);
        mCheckBoxVelX.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(velXseries);
                    }else {
                        graphView.removeSeries(velXseries);
                    }
                }
            }
        });

        // Checkbox for VELOCITY Y
        mCheckBoxVelY = (CheckBox) v.findViewById(R.id.checkBoxVelY);
        mCheckBoxVelY.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(velYseries);
                    }else {
                        graphView.removeSeries(velYseries);
                    }
                }
            }
        });

        // Checkbox for VELOCITY Z
        mCheckBoxVelZ = (CheckBox) v.findViewById(R.id.checkBoxVelZ);
        mCheckBoxVelZ.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(velZseries);
                    }else {
                        graphView.removeSeries(velZseries);
                    }
                }
            }
        });


        // Checkbox for POSITION all
        mCheckBoxPosAll = (CheckBox) v.findViewById(R.id.checkBoxPosAll);
        mCheckBoxPosAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    mCheckBoxPosX.setEnabled(true);
                    mCheckBoxPosY.setEnabled(true);
                    mCheckBoxPosZ.setEnabled(true);

                    if (mCheckBoxPosX.isChecked()) {
                        graphView.addSeries(posXseries);
                    }else {
                        graphView.removeSeries(posXseries);
                    }
                    if (mCheckBoxPosY.isChecked()) {
                        graphView.addSeries(posYseries);
                    }else {
                        graphView.removeSeries(posYseries);
                    }
                    if (mCheckBoxPosZ.isChecked()) {
                        graphView.addSeries(posZseries);
                    }else{
                        graphView.removeSeries(posZseries);
                    }
                }else{
                    mCheckBoxPosX.setEnabled(false);
                    mCheckBoxPosY.setEnabled(false);
                    mCheckBoxPosZ.setEnabled(false);
                    graphView.removeSeries(posXseries);
                    graphView.removeSeries(posYseries);
                    graphView.removeSeries(posZseries);
                }
            }
        });

        // Checkbox for POSITION X
        mCheckBoxPosX = (CheckBox) v.findViewById(R.id.checkBoxPosX);
        mCheckBoxPosX.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(posXseries);
                    }else {
                        graphView.removeSeries(posXseries);
                    }
                }
            }
        });

        // Checkbox for POSITION Y
        mCheckBoxPosY = (CheckBox) v.findViewById(R.id.checkBoxPosY);
        mCheckBoxPosY.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(posYseries);
                    }else {
                        graphView.removeSeries(posYseries);
                    }
                }
            }
        });

        // Checkbox for POSITION Z
        mCheckBoxPosZ = (CheckBox) v.findViewById(R.id.checkBoxPosZ);
        mCheckBoxPosZ.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isEnabled()) {
                    if (((CheckBox) v).isChecked()) {
                        graphView.addSeries(posZseries);
                    }else {
                        graphView.removeSeries(posZseries);
                    }
                }
            }
        });


        return v;
    }


    public void updateView(float[] quatAngle, float[] acceleration, float[] velocity, float[] position ) {

        double[] angle = new double[3];
        // Source for the calculation: http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        angle[0] = Math.toDegrees(Math.atan2(2.0 * (quatAngle[0] * quatAngle[1] + quatAngle[2] * quatAngle[3]), 1.0 - 2.0 * (quatAngle[1] * quatAngle[1] + quatAngle[2] * quatAngle[2])));
        angle[1] = Math.toDegrees(Math.asin(2.0 * (quatAngle[0] * quatAngle[2] - quatAngle[3] * quatAngle[1])));
        angle[2] = Math.toDegrees(Math.atan2(2.0 * (quatAngle[0] * quatAngle[3] + quatAngle[1] * quatAngle[2]), 1.0 - 2.0 * (quatAngle[2] * quatAngle[2] + quatAngle[3] * quatAngle[3])));

        // Plotting data on graphs
        boolean scroll = true;
        counter++;
        angleXseries.appendData(new GraphViewData(counter,angle[0]),scroll,NUM_POINTS+1);
        angleYseries.appendData(new GraphViewData(counter, angle[1]), scroll, NUM_POINTS+1);
        angleZseries.appendData(new GraphViewData(counter, angle[2]), scroll, NUM_POINTS+1);

        accXseries.appendData(new GraphViewData(counter,acceleration[0]),scroll,NUM_POINTS+1);
        accYseries.appendData(new GraphViewData(counter, acceleration[1]), scroll, NUM_POINTS+1);
        accZseries.appendData(new GraphViewData(counter, acceleration[2]), scroll, NUM_POINTS+1);

        velXseries.appendData(new GraphViewData(counter,velocity[0]),scroll,NUM_POINTS+1);
        velYseries.appendData(new GraphViewData(counter, velocity[1]), scroll, NUM_POINTS+1);
        velZseries.appendData(new GraphViewData(counter, velocity[2]), scroll, NUM_POINTS+1);

        posXseries.appendData(new GraphViewData(counter,position[0]),scroll,NUM_POINTS+1);
        posYseries.appendData(new GraphViewData(counter, position[1]), scroll, NUM_POINTS+1);
        posZseries.appendData(new GraphViewData(counter, position[2]), scroll, NUM_POINTS+1);

        if (!scroll){
            graphView.redrawAll();
        }
    }
}
